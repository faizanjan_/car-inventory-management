const { default: checkArg } = require("./argumentCheck.cjs");

let latestCar = inventory =>{
    if(!checkArg(inventory)) return [];
    inventory.sort((a,b)=>a.car_year-b.car_year)
    return inventory[inventory.length-1];
}

module.exports= latestCar;
