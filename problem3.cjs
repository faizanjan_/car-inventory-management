const checkArg= require("./argumentCheck.cjs");

let sortModels = inventory=>{
    if(!checkArg(inventory)) return [];

    inventory.sort((a,b)=>a.car_model.localeCompare(b.car_model));
    return inventory;
}

module.exports = sortModels;