const checkArg = inventory =>{
    if(!inventory || // check if no inventory is passed
        inventory.length===0 || // check if empty inventory is passed
        typeof(inventory)!=='object' || // check if inventory passed is an array
        typeof(inventory[0])!=='object')// check if inventory passed is an array of objects
    {
        return false;
    }
    else return true
}

module.exports = checkArg;