const problem4 = require("./problem4.cjs");
const checkArg= require("./argumentCheck.cjs");

const olderCars = (inventory, year)=>{
    if(!checkArg(inventory)) return [];

    const yearsList = problem4(inventory);
    const old_cars = [];
    for (let index=0; index<yearsList.length; index++){
        if(yearsList[index]<year) {
            old_cars.push(inventory[index])
        }
    }
    return old_cars;
}

module.exports= olderCars;