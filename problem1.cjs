const checkArg= require("./argumentCheck.cjs");


let getCarById = (inventory,id) => {
    if(!checkArg(inventory) || id==='undefined' || typeof(id)!=='number') return [];

    for(let carNum=0; carNum<inventory.length; carNum++){
        if(inventory[carNum].id===id) return inventory[carNum];
    }
}

module.exports = getCarById;