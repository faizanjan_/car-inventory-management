const checkArg= require("./argumentCheck.cjs");

const getLastCar = inventory =>{
    if(!checkArg(inventory)) return [];

    else return inventory[inventory.length-1]
}

module.exports = getLastCar;