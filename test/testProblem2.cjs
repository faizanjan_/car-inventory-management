const inventory = require("../cars_inventory.cjs");
const getLastCar = require("../problem2.cjs");

const result = getLastCar('string');

if(result && result.length!==0) {
    console.log(`Last car is a ${result.car_make} ${result.car_model}`)
}
else console.log("Pass proper arguments")