let inventory = require('../cars_inventory.cjs');
let latestCar = require('../latestCar.cjs')

let result = latestCar(inventory);
if(result && result.length!==0) {
    console.log(`Latest car is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}
else console.log("Pass proper arguments");