const inventory = require('../cars_inventory.cjs');
const getCarById = require('../problem1.cjs');

let id = 33;
const result = getCarById(inventory,23);
if(result.length!==0) {
    console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`)
}
else console.log("Pass proper arguments")