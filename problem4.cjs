const checkArg= require("./argumentCheck.cjs");

let yearList = inventory=>{
    if(!checkArg(inventory)) return [];


    const years = [];
    for (let obj of inventory){
        years.push(obj.car_year);
    }
    return years;
}

module.exports = yearList;