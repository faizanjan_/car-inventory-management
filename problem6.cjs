const checkArg= require("./argumentCheck.cjs");

const getBMWAndAudi= inventory=>{
    if(!checkArg(inventory)) return [];

    let BMWAndAudi = [];
    for(let car of inventory){
        if(car.car_make==='Audi' || car.car_make==='BMW'){
            BMWAndAudi.push(car);
        }
    }
    return BMWAndAudi;
}

module.exports = getBMWAndAudi;